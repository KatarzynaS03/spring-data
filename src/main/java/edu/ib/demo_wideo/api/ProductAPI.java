package edu.ib.demo_wideo.api;

import edu.ib.demo_wideo.repo.ProductRepository;
import edu.ib.demo_wideo.repo.entity.Product;
import edu.ib.demo_wideo.service.ProduktService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ProductAPI {

    private ProductRepository productRepository;
    private ProduktService produktService;

    @Autowired
    public ProductAPI (ProduktService produktService) {
        this.produktService = produktService;
    }

    @GetMapping("/product/all")
    public Iterable<Product> getALL(){

        return produktService.findALL();
    }
    @GetMapping("/product")
    public Optional<Product> getById(@RequestParam Long index){
        return produktService.FindById(index);
    }

    @PostMapping("/admin/product")
    public Product addVIdeo(@RequestBody Product product){
        return produktService.Save(product);
    }

    @PutMapping("/admin/product")
    public Product updateVideo(@RequestBody Product product){
        return produktService.Save(product);
    }
    @DeleteMapping("/product")
    public void deleteVideo(@RequestParam Long index){
        produktService.delete(index);
    }

    @Data
    public class ProductDto {
        private String mark;
        private float price;
        private boolean available;
    }

    @PatchMapping("/admin/product/{index}")
    public void patchProduct(
            @PathVariable  long index,
            @RequestBody ProductDto productDto)  {
        Product product = produktService
                .FindById(index).get();

        boolean needUpdate = false;

        if (StringUtils.hasLength(productDto.getMark())) {
            product.setMark(productDto.getMark());
            needUpdate = true;
        }

        if (product.getPrice()>0) {
            product.setPrice(productDto.getPrice());
            needUpdate = true;
        }
        if (StringUtils.hasLength(String.valueOf(productDto.isAvailable()))) {
            productDto.setAvailable(productDto.isAvailable());
            needUpdate = true;
        }

        if (needUpdate) {
            productRepository.save(product);
        }
    }
}
