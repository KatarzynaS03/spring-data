package edu.ib.demo_wideo.Generate;

import edu.ib.demo_wideo.repo.entity.Customer;
import edu.ib.demo_wideo.repo.entity.Product;
import edu.ib.demo_wideo.repo.entity.Zamowienie;
import edu.ib.demo_wideo.repo.CustomerRepository;
import edu.ib.demo_wideo.repo.ZamowienieRepository;
import edu.ib.demo_wideo.repo.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Component
public class DbMockData {
    private ProductRepository productRepository;
    private ZamowienieRepository orderRepository;
    private CustomerRepository customerRepository;

    @Autowired
    public DbMockData(ProductRepository productRepository, ZamowienieRepository orderRepository, CustomerRepository customerRepository) {
        this.productRepository = productRepository;
        this.orderRepository = orderRepository;
        this.customerRepository = customerRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fill() {
        Product product = new Product("Korek", 2.55f, true);
        Product product1 = new Product("Rura", 5f, true);
        Customer customer = new Customer("Jak Kowalski", "Wrocław");
        Set<Product> products = new HashSet<>() {
            {
                add(product);
                add(product1);
            }};
        Zamowienie order = new Zamowienie(customer, products, LocalDateTime.now(), "in progress");

        productRepository.save(product);
        productRepository.save(product1);
        customerRepository.save(customer);
        orderRepository.save(order);
    }
}