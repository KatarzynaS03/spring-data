package edu.ib.demo_wideo.Generate;

import edu.ib.demo_wideo.repo.ProductRepository;
import edu.ib.demo_wideo.repo.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class Start {

    private ProductRepository carRepo;

    @Autowired
    public Start(ProductRepository carRepoc){
        this.carRepo = carRepoc;
    }

    @EventListener (ApplicationReadyEvent.class)
    public void runExample(){
        Product car = new Product("Honda", 1234, true);
        carRepo.save(car);
        Product car1 = new Product("Honda", 12345, false);
        Product car2 = new Product("Toyota", 9182, true);
        Product car3 = new Product("Mercedes", 34, false);
        Product car4 = new Product("Audi", 9182, true);
        carRepo.save(car1);
        carRepo.save(car2);
        carRepo.save(car3);
        carRepo.save(car4);

        Iterable<Product> all = carRepo.findAll();
        all.forEach(System.out::println);

    }
}
