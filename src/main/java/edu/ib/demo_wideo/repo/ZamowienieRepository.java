package edu.ib.demo_wideo.repo;

import edu.ib.demo_wideo.repo.entity.Zamowienie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ZamowienieRepository extends CrudRepository<Zamowienie, Long> {
}
