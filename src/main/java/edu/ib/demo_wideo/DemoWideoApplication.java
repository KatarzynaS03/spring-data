package edu.ib.demo_wideo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoWideoApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemoWideoApplication.class, args);
    }

}
